const config = {
  showDebug: false,
};

// keep track of current face positions here
let faces = [];

// short cut to find elements on html page
function el(id) {
  return document.getElementById(id);
}

function render() {
  const { width, height } = el('video-output');
  const ctx = el('video-output').getContext('2d');
  ctx.drawImage(el('video-input'), 0, 0, width, height);
  drawFaces(ctx, faces);
  window.requestAnimationFrame(render);
}

function initWebCam() {
  const input = el('video-input');
  try {
    navigator.mediaDevices.getUserMedia({ audio: false, video: true })
      .then(stream => input.srcObject = stream)
      .catch(error => console.log(error));
  }
  catch (e) {
    alert('No media, no fun');
  }
}

function startLoop() {
  render();
}

function onFaceDetect(e) {
  if (e.data.length) faces = e.data;
}

function initTracker() {
  const tracker = new tracking.ObjectTracker('face');
  tracker.setInitialScale(4);
  tracker.setStepSize(2);
  tracker.setEdgesDensity(0.1);

  tracker.on('track', onFaceDetect);
  tracking.track('#video-input', tracker);
}

function saveImage() {
  const link = el('save-image');
  link.href = el('video-output').toDataURL();
  link.download = 'goodtimes.png';
}

function changeMedia(e) {
  const webcam = e.target.value === 'webcam';
  const video = el('video-input');
  if (!webcam) {
    console.log('video');
    video.srcObject = null;
    video.src = '../assets/ricky.mp4';
  }
  else {
    initWebCam();
  }
}

function saveImage() {
  const link = el('save-image');
  link.href = el('video-output').toDataURL();
  link.download = 'goodtimes.png';
}

function init() {
  initWebCam();
  initTracker();
  el('media').onchange = changeMedia;
  el('save-image').onclick = saveImage;
  startLoop();
}

window.onload = init;
