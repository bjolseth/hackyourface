// Low pass filters / smoothens jumpy values
// Setting the alpha lower makes the filter more stable but slower
class LowPass {

  constructor(alpha = 0.15) {
    this.alpha = alpha;
    this.value = null;
  }

  filter(newValue) {
    if (this.value === null) {
      this.value = newValue; // use first value as initial value
    }
    this.value = this.value + this.alpha * (newValue - this.value);

    return this.value;
  }
}

let decorBall, decorGlasses;

const smoothenX = new LowPass();
const smoothenY = new LowPass();
const smoothenW = new LowPass();
const smoothenH = new LowPass();

function drawFaces(ctx, faces) {
  if (config.showDebug) {
    faces.forEach(face => drawFaceRect(ctx, face));
  }
  // faces.forEach(face => drawDecor(ctx, face));
  faces.forEach(face => drawNose(ctx, face));
  // faces.forEach(face => drawPigNose(ctx, face));
  // faces.forEach(face => drawHair(ctx, face));
}

function drawNose(ctx, face) {
  const { x, y, width, height } = face;
  const xF = smoothenX.filter(x);
  const yF = smoothenY.filter(y);
  const wF = smoothenW.filter(width);
  const hF = smoothenH.filter(height);
  // ctx.drawImage(decorBall, xF, yF, 50, 50);
  ctx.drawImage(decorBall, xF + wF/2 - 20, yF + hF/2, 50, 50);
}

function drawHair(ctx, face) {
  const { x, y, width, height } = face;
  const xF = smoothenX.filter(x);
  const yF = smoothenY.filter(y);
  const wF = smoothenW.filter(width);
  const hF = smoothenH.filter(height);
  // ctx.drawImage(decorBall, xF, yF, 50, 50);
  ctx.drawImage(decorHair, xF - 20, yF - 90, wF * 1.22, hF * 1.2);
}


function drawPigNose(ctx, face) {
  const { x, y, width, height } = face;
  const xF = smoothenX.filter(x);
  const yF = smoothenY.filter(y);
  const wF = smoothenW.filter(width);
  const hF = smoothenH.filter(height);
  // ctx.drawImage(decorBall, xF, yF, 50, 50);
  ctx.drawImage(decorPig, xF + wF/2 - 35, yF + hF/2 - 20, 70, 70);
}

function drawDecor(ctx, faceRect) {
  const { x, y, width, height } = faceRect;
  const xF = smoothenX.filter(x);
  const yF = smoothenY.filter(y);
  const wF = smoothenW.filter(width);
  const hF = smoothenH.filter(height);
  ctx.drawImage(decorGlasses, xF, yF + 20, wF, hF);
}

function loadImage(url) {
  const image = new Image();
  image.src = url;
  return image;
}

// images rendered on the canvas need to be loaded first
function loadImages() {
  decorGlasses = loadImage('./objects/glasses.png');
  decorBall = loadImage('./objects/nose.png');
  decorPig = loadImage('./objects/pignose.png');
  decorHair = loadImage('./objects/trumphair.png');
}

loadImages();
