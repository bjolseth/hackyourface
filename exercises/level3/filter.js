/**
 * TIP: drawimages on canvas: canvasContext.drawImage(image, x, y, width, height)
 * https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
 */

let pigNose;

/**
 * Called for each video frame
 * faces is a list of objects with rectangles for each face detected
 * Each rectangle is an object that contains x, y, width and height for the face
 * canvasContext is your 'pen' to draw on the canvas
 */
function drawFaces(canvasContext, faces) {
  for (let i = 0; i < faces.length; i++) {
    drawFaceRect(canvasContext, faces[i]);

    // YOUR CODE HERE
  }
}

// Just for drawing squares around the faces
function drawFaceRect(canvasContext, face) {
  canvasContext.beginPath();
  canvasContext.strokeStyle = '#090';
  canvasContext.lineWidth = 3;
  canvasContext.rect(face.x, face.y, face.width, face.height);
  canvasContext.stroke();
}

// Load images, eg pig nose, trump hair etc, so they can be used in the canvas
function loadImage(url) {
  const image = new Image();
  image.src = url;
  return image;
}

// images rendered on the canvas need to be loaded first
// add your own images here too
function loadImages() {
  pigNose = loadImage('./objects/pignose.png');
}

loadImages();
