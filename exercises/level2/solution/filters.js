function limit(value, min, max) {
  return Math.min(Math.max(min, value));
}

function l255(value) {
  return limit(value, 0, 255);
}

// for dat.gui settings panel:
const settings = {
  halfToneThreshold: 120,
  lightColor: [255, 255, 255],
  darkColor: [0, 0, 0],
  brighten: 100,
};

// not part of the task, but always nice to have an easy way to tune algorithms for engineers
function defineSettings() {
  const gui = new dat.GUI();
  const f1 = gui.addFolder('Half tone');
  f1.add(settings, 'halfToneThreshold', 0, 255);
  f1.addColor(settings, 'lightColor');
  f1.addColor(settings, 'darkColor');
  const f2 = gui.addFolder('Brighten');
  f2.add(settings, 'brighten', 0, 255);
};

function copy(inputFrame, outputFrame) {
  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const pixel = inputFrame.get(x, y);
      const newPixel = {
        r: pixel.r,
        g: pixel.g,
        b: pixel.b,
        a: pixel.a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

function mirror(inputFrame, outputFrame) {
  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const pixel = inputFrame.get(x, y);
      let newPixel = pixel;
      const centerX = inputFrame.width / 2;
      if (x > centerX) {
        const opposite = inputFrame.get(inputFrame.width - x, y);
        newPixel = opposite;
      }
      outputFrame.set(x, y, newPixel);
    }
  }
}

function invert(inputFrame, outputFrame) {
  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const { r, g, b, a} = inputFrame.get(x, y);
      const newPixel = {
        r: 255 - r,
        g: 255 - g,
        b: 255 - b,
        a: a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

function sepia(inputFrame, outputFrame) {
    // https://www.dyclassroom.com/image-processing-project/how-to-convert-a-color-image-into-sepia-image
  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const { r, g, b, a} = inputFrame.get(x, y);
      const newPixel = {
        r: l255(0.393 * r + 0.769 * g + 0.189 * b),
        g: l255(0.349 * r + 0.686 * g+ 0.168 * b),
        b: l255(0.272 * r + 0.534 * g + 0.131 * b),
        a: a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

function brighten(inputFrame, outputFrame) {
  const increase = settings.brighten;

  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const { r, g, b, a} = inputFrame.get(x, y);
      const newPixel = {
        r: l255(r + increase),
        g: l255(g + increase),
        b: l255(b + increase),
        a: a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

function gray(inputFrame, outputFrame) {

  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const { r, g, b, a} = inputFrame.get(x, y);
      const gray = (r + g + b) / 3;
      const newPixel = {
        r: gray,
        g: gray,
        b: gray,
        a: a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

function halftone(inputFrame, outputFrame) {
  const threshold = settings.halfToneThreshold;

  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const { r, g, b, a} = inputFrame.get(x, y);
      const average =  (r + g + b) / 3;
      const color = average > threshold ? settings.lightColor : settings.darkColor;
      const newPixel = {
        r: color[0],
        g: color[1],
        b: color[2],
        a: a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

// add your filter functions here, the dropdown on the page will automatically show them
const filters = {
  copy,
  gray,
  brighten,
  invert,
  sepia,
  halftone,
  mirror,
};
