// define constants for our project
const config = {
  filter: Object.keys(filters)[0],
};

// short cut to find elements on html page
function el(id) {
  return document.getElementById(id);
}

function getCurrentVideoFrame() {
  // copy the bits to an invisible canvas, then they become available
  const ctx = el('video-input-copy').getContext('2d');
  const { width, height } = el('video-input-copy');
  ctx.drawImage(el('video-input'), 0, 0, width, height);
  return ctx.getImageData(0, 0, width, height);
}

function render() {
  const source = getCurrentVideoFrame();
  const { width, height } = el('video-input-copy');
  const target = el('video-output').getContext('2d').getImageData(0, 0, width, height);
  const currentFilter = filters[config.filter];
  currentFilter(source.data, target.data, { width, height });
  el('video-output').getContext('2d').putImageData(target, 0, 0);
  window.requestAnimationFrame(render);
}

function initWebcam() {
  const input = el('video-input');
  try {
    navigator.mediaDevices.getUserMedia({ audio: false, video: true })
      .then(stream => input.srcObject = stream)
      .catch(error => console.log(error));
  }
  catch (e) {
    alert('No media, no fun');
  }
}

function populateFilters() {
  const box = el('select-filter');
  Object.keys(filters).forEach(filter => {
    const option = document.createElement('option');
    option.value = filter;
    option.innerText = filter;
    box.appendChild(option);
  })
  box.onchange = e => config.filter = e.target.value;
}

function changeMedia(e) {
  const webcam = e.target.value === 'webcam';
  const video = el('video-input');
  if (!webcam) {
    console.log('video');
    video.srcObject = null;
    video.src = '../assets/cisco.mp4';
  }
  else {
    initWebcam();
  }
}

function init() {
  populateFilters();
  initWebcam();
  render();
  // el('media').onchange = changeMedia;
  if (typeof defineSettings === 'function') {
    defineSettings();
  }
}

window.onload = init;
