// define constants for our project
const config = {
  filter: Object.keys(filters)[0],
};

let currentFrame;
let currentOutputFrame;
let frameCount = 0;

// short cut to find elements on html page
function el(id) {
  return document.getElementById(id);
}

function constructFrameObject() {
  const { width, height } = el('video-input-copy');
  currentFrame = new Frame(width, height);
  currentOutputFrame = new Frame(width, height);
}

function getCurrentVideoFrame() {
  // copy the bits to an invisible canvas, then they become available
  const ctx = el('video-input-copy').getContext('2d');
  const { width, height } = el('video-input-copy');
  ctx.drawImage(el('video-input'), 0, 0, width, height);
  return ctx.getImageData(0, 0, width, height);
}

function render() {
  const source = getCurrentVideoFrame();
  currentFrame.updatePixels(source.data);
  const { width, height } = el('video-input-copy');
  const target = el('video-output').getContext('2d').getImageData(0, 0, width, height);
  const currentFilter = filters[config.filter];
  currentOutputFrame.pixelList = target.data; // make sure they are linked
  currentFilter(currentFrame, currentOutputFrame);
  el('video-output').getContext('2d').putImageData(target, 0, 0);
  window.requestAnimationFrame(render);
  frameCount++;
}

function updateFPS() {
  const fps = frameCount;
  frameCount = 0;
  el('fps').innerText = fps;
}

function initWebCam() {
  const input = el('video-input');
  try {
    navigator.mediaDevices.getUserMedia({ audio: false, video: true })
      .then(stream => input.srcObject = stream)
      .catch(error => console.log(error));
  }
  catch (e) {
    alert('No media, no fun');
  }
}

function populateFilters() {
  const box = el('select-filter');
  Object.keys(filters).forEach(filter => {
    const option = document.createElement('option');
    option.value = filter;
    option.innerText = filter;
    box.appendChild(option);
  })
  box.onchange = e => config.filter = e.target.value;
}

function changeMedia(e) {
  const webcam = e.target.value === 'webcam';
  const video = el('video-input');
  if (!webcam) {
    console.log('video');
    video.srcObject = null;
    video.src = '../assets/cisco.mp4';
  }
  else {
    initWebCam();
  }
}

function saveImage() {
  const link = el('save-image');
  link.href = el('video-output').toDataURL();
  link.download = 'goodtimes.png';
}

function init() {
  populateFilters();
  constructFrameObject();
  initWebCam();
  el('media').onchange = changeMedia;
  el('save-image').onclick = saveImage;
  setInterval(updateFPS, 1000);
  render();
  if (typeof defineSettings === 'function') {
    defineSettings();
  }
}

window.onload = init;
