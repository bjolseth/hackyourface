// The input frame is the source video
// The output frame is the filtered video you should make
function copy(inputFrame, outputFrame) {
  for (let y = 0; y < inputFrame.height; y++) {
    for (let x = 0; x < inputFrame.width; x++) {
      const pixel = inputFrame.get(x, y);
      const newPixel = {
        r: pixel.r,
        g: pixel.g,
        b: pixel.b,
        a: pixel.a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

// add your code here, and add the function to filters below
function grayscale(inputFrame, outputFrame) {

}

// add your filter functions here, the dropdown on the page will automatically show them
const filters = {
  copy,
  // grayscale
};
