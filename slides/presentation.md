
# JavaScript: Hack Your Face

Tore Bjølseth &lt;tbjolset@cisco.com&gt;

## Structure

* A quick intro / crash course to JavaScript
* Hands-on coding exercises
* Theme: image processing
* More focus on fun and play than theory

Warning: The code we show in this course is not safe for work.

## Content

* Level 0: About the language
* Level 1: Variables, Functions, if...else
* Level 2: Callbacks, Arrays, Bitmaps
* Level 3: Objects, Error handling, Classes

## Exercises

* Browsers: Recommend Firefox or Safari
* Editor: You choose. Atom or Visual Studio code is fine
* At the end of each level is a code challenge
* Do them in groups of 2 or 3 if you can
* A proposed solution is in the solution/ sub-folders
* Read the hints in the code


## About this presentation

* It is made in JavaScript...

* ...by peknudse@cisco.com

* You can view it in your browser...

* ...and hack it

* Double click [slides/index.html](../slides/index.html) to view presentation


## Questions?

* Unmute your self and ask

* Or ask in the chat window

* Or use the 'Raise hand' feature

## Competition

* Take a picture, screenshot or video today

* Post on Instagram or Twitter, tag #cisconorge

* Or email to agjerlau@cisco.com

* Win a fabulous secret prize


# Level 0

## Language features

* Core web technology together with HTML and CSS
* Especially popular for dynamic web pages
* High-level scripting language, not compiled
* Single threaded with an implicit event loop
* Dynamically typed
* First class functions

## Short history

* Created by Brendan Eich at Netscape in 1995
* Written in ten days (!)
* Name and syntax inspired by Java, but different semantics
* Ajax (XHR): Gmail 2004
* Browser wars: ~2000-2010
* Server side JavaScript in Node since 2009
* Everywhere: Laptops, phones, tablets, TVs, Cisco video devices, ...

## Rise in popularity

* [Assembly language for the Web](https://www.hanselman.com/blog/JavaScriptIsAssemblyLanguageForTheWebSematicMarkupIsDeadCleanVsMachinecodedHTML.aspx)
* High-speed, performant JIT VMs: [V8](https://v8.dev/), [SpiderMonkey](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey), [JavaScriptCore](https://developer.apple.com/documentation/javascriptcore)
* Pervasive: Frontend, backend
* State-of-the-art tooling
* Compilation target for other languages

## Today

Web pages don't look like web pages no more:

* [Fluid Simulation](https://paveldogreat.github.io/WebGL-Fluid-Simulation/) using WebGL
* [The Office](https://editor.archilogic.com/3d/archilogic/nsh7wfvq?sceneId=3d6acff4-abcd-4c58-b3f3-d56d99642199&mode=view&view-menu=none&main-menu=none) in 3D
* [Redax Sounds](https://redaxsounds.netlify.app/) - ambient sound generator w Web Audio


## A Simple page

```html
<html>
<body>
  <button id="start" onclick="start()">Ready player 1</button>
</body>
<script>
  function start() {
    alert('Play!');
  }
</script>
</html>
```

## Hello world

* Let's do Hello word in 4 different ways together

* Open [exercises/level0/index.html](../exercises/level0/index.html) in your editor and browser

Demo: ../exercises/level0/index.html Show exercise


# Level 1

## Variables

- **let** can be changed:

```javascript
let coins = 0;
coins = 22; // ok
```

- **const** cannot be changed:

```javascript
const pi = 3.14;
pi = 9.81; // error
```

- Old school

```javascript
var dont = 'use me no more';
```

## Types

```javascript
const name = 'mario'; // string
const lives = 3;      // number
const isBig = true;   // bool
const player = {
  name: name,
  lives: lives,
  isBig: isBig,
};                    // object
function run() {...}  // function
const boss = null;    // null
const dragon;         // undefined
```

* You can change types implicitly

```javascript
let brother = 'luigi';
brother = 42;
```

## Functions

* Functions use a familiar syntax:

```javascript
function pythagoras(a, b) {
  return Math.sqrt(a*a + b*b);
}
console.log('c=', pythagoras(3, 4)); // prints 5
```

## Functions

* Functions can be assigned to variables:

```javascript
jumpButton.onclick = function jump(height) {
  console.log('jump', height);
};
```

* The same, but with fat arrow syntax:

```javascript
jumpButton.onclick = (height) => console.log('jump', height);
```

## Strings

* Implicit coercion: [wat?](https://www.destroyallsoftware.com/talks/wat)

```javascript
console.log("foo" + 1337 + "bar")
// => foo1337bar
```

* Concatenation using "+"
* Supports formatting strings using backticks:

```javascript
const place = "World";
console.log(`Hello, ${place}!`);
// => Hello, World!
```

## if, else, while, ...

* Flow controls are similar to other languages:

```javascript
let marioIsBig = true;
let lives = 3;
// ...
while (lives >= 0) {
  if (marioIsBig) {
    marioIsBig = false;
  }
  else {
    lives--;
  }
}
```

## Updating a web page

```html
<html>
<style>
  button {
    background-color: blue;
    color: white;
  }
</style>
<body>
  <button id="start">Ready player 1</button>
</body>
<script>
  const button = document.getElementById('start');
  button.style.backgroundColor = 'orange';
</script>
</html>
```

## Web Colours - RGB(A)

* Each pixel has four components: channels

* The amount of red (R), green (G) and blue (B) (0-255)

* The amount of opacity (A for alpha) (0-255)

* White: all colour channels are 255

* Black: all colour channels are 0

* Alpha: 255 is fully opaque, 0 is fully transparent

## Documentation & debugging

* [MDN Web Docs](https://developer.mozilla.org/en-US/) - Comprehensive developer documentation
* [devdocs.io](https://devdocs.io/) - Quick, fuzzy API searching

Debugging tools are integrated in every major browser:

<img src="../exercises/assets/debugger.png"/>

## Ready Player 1

* Open [exercises/level1/index.html](../exercises/level1/index.html) in your editor and browser

* Make the sliders change the color box

* Share code? Share your screen...

* ... Or use https://codeshare.io

Demo: ../exercises/level1/index.html Show exercise

# Level 2

## Callbacks

* In JavaScript, functions are first class citizens

* They can be passed as arguments and stored as other variables

* Callbacks are useful when dealing with events and "slow" (async) operations:

* JavaScript is a single threaded language

```javascript
function showWeather(temperature) {
  alert("Tomorrow's temperature:" + temperature);
}
network.request('yr.no/getForecast', showWeather); // Note: no parenthesis
```

## Timers

* Single timer

```javascript
function changeTitle(newTitle) {
  document.title = title;
}
setTimeout(() => changeTitle('Super Mario'), 2000);
```

* Repeating timer

```javascript
let coins = 0;
function collectCoin() {
  console.log(coins++);
  if (coins > 10) {
    clearInterval(timer); // stops the timer
  }
}
const timer = setInterval(collectCoin, 300);
```

## Arrays and loops

* Create an array:

```javascript
const myPixels = [123, 345, 225, 12, 345];
```

* Traverse an array:

```javascript
for (let i = 0; i < myPixels.length; i++) {
  console.log(myPixels[i]);
}
// Or:
for (let value of myPixels) {
  console.log(value);
}
// Or:
myPixels.forEach((value) => console.log(value));
```

## User media

* Get access to the web camera
* Used for WebRTC (web video calls)
* Requires HTTPS

```javascript
<video id="myVideo"></video>
<script>
  const video = document.querySelector('video');
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: true })
    .then((stream) => { video.srcObject = stream }); // "promise"
</script>
```

## Bitmap array

* Four values per pixel: red, green, blue, alpha
* Flat list
* Left to right, top to bottom
* Grayscale: Red, Green and Blue are equal

Four pixel bitmap: <img src="../exercises/assets/pixels.png" height="50px" />

```javascript
const myBitmap = [
  255, 0, 0, 255,
  0, 255, 0, 255,
  0, 0, 255, 255,
  128, 128, 128, 255,
];
```

## Ready player 1

* Open exercises/level2/filter.js
* Create a grayscale filter

Not able to see web cam?

* Try turning off the video in your Webex client
* Try different browser
* Try the recorded video instead

Demo: ../exercises/level2/index.html Show exercise

## Run local web server (Mac/Linux)

* Open terminal, go to your exercises/ folder
* python -m SimpleHTTPServer 8080

or

* python3 -m http.server 8080
* Then go to localhost:8080 in your browser



## Self study: Convolution filters

* Simple matrix to alter the pixel based on nearby pixels

```javascript
// Example: sharpen filter
const matrix = [
  0, -1, 0,
  -1, 5, -1,
  0, -1, 0,
];
const newImage = matrixMultiply(allImagePixels, matrix);
```

* [html5rocks.com/en/tutorials/canvas/imagefilters/](https://www.html5rocks.com/en/tutorials/canvas/imagefilters/)

Demo: ../exercises/level2/convolution/index.html Demo

# Level 3

## Objects

* Use {} to construct
* Provides key/value pairs
* Can be nested
* Access properties with dot

```javascript
const enemy23 = {
  type: 'duck',
  position: {
    x: 22,
    y: 147,
  },
  alive: true,
};
enemy23.position.x += 2;
```

## Objects

* Shorthand property names (ES2015):

```javascript
const x = 10, y = 20;
const pos = { x: x, y: y };
const pos2 = { x, y }; // the same as above
```

* Object 'destructuring':

```javascript
function updatePos(pos) {
  const x = pos.x;
  const y = pos.y;
  console.log(x, y);
}
function updatePos2({ x, y }) {
  console.log(x, y); // shortcut, exactly the same as above
}
```

## Exception handling

* JavaScript can throw any type as an exception:

```javascript
function setScore(score) {
  if (typeof score !== 'number') {
    throw new Error('setScore requires a number');
  }
  // ...
}
```

* Usage:

```javascript
try {
  setScore('luigi');
  playFanfare(); // won't be called
}
catch (e) {
  console.error('Game over', e);
}
```

## Classes

* With EcmaScript 6, JavaScript finally has class support:

```javascript
class Level {
  constructor(points) {
    this.points = points;
  }
  increasePoints(increment) {
    this.points += incremenet;
  }
}
const level3 = new Level(120);
level3.increasePoints(30);
console.log(level3.points); // 150
```


## Canvas

Use to draw graphics on screen, such as circles, rectangles and images.

```javascript
<canvas id="game" width="300" height="300"></canvas>
<script>
  const c = document.getElementById("game");
  const ctx = c.getContext("2d");
  ctx.moveTo(0, 0);
  ctx.lineTo(200, 100);
  ctx.strokeStyle = 'red';
  ctx.stroke();
</script>
```

## tracking.js

* JavaScript libraries are often free and easy to use
* Lets you do something you wouldn't be able to do yourself
* Or saves you time
* tracking.js helps you track face, eyes and mouth

Demo: https://www.npmjs.com/package/tracking NPM libraries for JavaScript


## Ready player 1

* Open [exercises/level3/filter.js](../exercises/level3/filter.js)
* Find some images to put on your face. Pig nose, funny glasses, be creative
* Draw and scale the images on your face(s)

Bonus levels:

* Stabilise the objects
* Switch faces on two persons

Demo: ../exercises/level3/index.html Show exercise



## We skipped a lot

* Scoping, closures
* HTML elements, the Document Object Model (DOM)
* Promises, async
* Built-ins: String, Date, Array, Math, ...
* Making HTTP requests
* SVG, WebGL, Web Audio
* Transpilers
* Node.js (server side JavaScript)
* Libraries: jQuery, React, Angular, BootStrap, ...


## Thank you!

<img class="center" src="../exercises/assets/best-place.png"/>

Looking for internship, summer job, full time job? Go here:

<p style="text-align: center">
  <a href="https://cisco.avature.net/norwayevents">cisco.avature.net/norwayevents</a>
</p>
