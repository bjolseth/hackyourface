let currentSlideIndex = document.location.hash.startsWith("#slide-") ? parseInt(document.location.hash.substr(7)) - 1 : 0;
let fireworkIntervalId = -1;

const slideElement = document.querySelector("#slide");
const progressHandleElement = document.querySelector("#progress-handle");
const progressBackgroundElement = document.querySelector("#progress-background");
const soundNextSlideElement = document.querySelector("#sound-next-slide");
const soundLastSlideElement = document.querySelector("#sound-last-slide");
const soundLevel = document.querySelector('#sound-level');
const muteIconElement = document.querySelector("#mute-icon");
const slidePositionElement  = document.querySelector("#slide-position");
const soundWarningElement = document.querySelector("#sound-warning");
const fireworkElement = document.querySelector("#progress-firework");

muteIconElement.style.display = 'none';

progressHandleElement.addEventListener("transitionend", (event) => {
    progressHandleElement.classList.remove("progress-handle-jump");
});

function updateCurrentSlide() {
    const slide = slides[currentSlideIndex];
    let html = "None"
    if (slide.type === 'cover') {
        html = `<h1 class="slide-cover-title"><marquee scrollamount="20">${slide.title}</marquee></h1><h2 class="slide-cover-author">${slide.author}</h2>`;
    } else if (slide.type === 'section') {
        html = `<h1 class="slide-section-title">${slide.title}</h1>`
    } else if (slide.type === 'content') {
        html = `<h1 class="slide-content-title">${slide.title}</h1><div id="slide-content-content">${slide.content}</div>`;
    }
    if (slide.demo) {
        if (slide.demo.url.startsWith("videos/")) {
            html += `<a class="slide-content-demo-video" target="_blank" href="javascript:showVideo(\'${slide.demo.url}\')">Demo: ${slide.demo.text}</a>`;
        } else {
            html += `<a class="slide-content-demo" href="${slide.demo.url}">${slide.demo.text}</a>`;
        }
    }
    slideElement.innerHTML = html;

    let progress = ~~(currentSlideIndex * 100/ (slides.length - 1));
    progressHandleElement.style.left = (1 + progress * 0.84) + '%';
    progressBackgroundElement.style.left = (-progress*2.99) + '%';
    progressHandleElement.classList.add("progress-handle-jump");

    if (slide.title.toLowerCase().includes('level')) {
      setTimeout(() => soundLevel.play(), 1200);
    }

    const slideNumber = currentSlideIndex + 1;
    slidePositionElement.innerHTML = slideNumber + "-" + slides.length;
    document.location.hash = "#slide-" + slideNumber;
}

function isMuted() {
    return muteIconElement.style.display == 'inline-block';
}
function toggleMute() {
    muteIconElement.style.display = isMuted() ? 'none' : 'inline-block';
}

function showFireworks() {
    if (fireworkIntervalId == -1) {
        clearInterval(fireworkIntervalId);
    }
    fireworkElement.style.display = 'inline';
    fireworkElement.src = '';
    fireworkIntervalId = setInterval(() => {
        const x = 100 + window.innerWidth * 0.8 * Math.random();
        const y = 100 + window.innerHeight * 0.7 * Math.random();
        fireworkElement.style.left = x + 'px';
        fireworkElement.style.top = y + 'px';
        fireworkElement.src = 'assets/firework.gif?=' + Math.random() * 1000;
    }, 750);
}

function hideFireworks() {
    clearInterval(fireworkIntervalId);
    fireworkIntervalId = -1;
    fireworkElement.style.display = 'none';
}

function nextSlide() {
    if (currentSlideIndex < slides.length - 1) {
        currentSlideIndex++;
        updateCurrentSlide();
        if (!isMuted()) {
            if (currentSlideIndex == slides.length - 1) {
                soundLastSlideElement.play();
                showFireworks();
            } else {
                soundNextSlideElement.play();
            }
        }
    }
}

function goToFirstSlide() {
    hideFireworks();
    currentSlideIndex = 0;
    updateCurrentSlide();
}

function previousSlide() {
    if (currentSlideIndex > 0) {
        currentSlideIndex--;
        updateCurrentSlide();
    }
}

function startDemo() {
    const demoLinkElement = document.querySelector("a");
    console.log(demoLinkElement);
    if (demoLinkElement) {
        console.log("CLICK!");
        demoLinkElement.click();
    }
}

function closeVideo() {
    const videoPlayer = document.querySelector(".video-player");
    if (videoPlayer) {
        document.body.removeChild(videoPlayer);
    }
}

function showVideo(url) {
    const videoPlayerDiv = document.createElement("div");
    videoPlayerDiv.classList.add("video-player");
    const videoElement = document.createElement("video");
    videoElement.controls = true;
    videoElement.src = url;
    videoPlayerDiv.appendChild(videoElement);
    document.body.appendChild(videoPlayerDiv);
    videoElement.play();
}

function isShowingVideo() {
    return document.querySelector(".video-player") != null;
}

function toggleVideoPlayState() {
    const videoElement = document.querySelector("video");
    if (videoElement) {
        if (videoElement.paused) {
            videoElement.play();
        } else {
            videoElement.pause()
        }
    }
}

window.addEventListener("keydown", (event) => {
    let preventDefault = true;
    switch (event.keyCode) {
        case 36: goToFirstSlide(); break;
        case 37: previousSlide(); break;
        case 39: nextSlide(); break;
        case 13: startDemo(); break;
        case 27: goToFirstSlide(); break;
        case 32: {
            if (isShowingVideo()) {
                toggleVideoPlayState();
            } else {
                toggleMute();
            }
            break;
        }
        default: preventDefault = false;
    }
    if (preventDefault) {
        event.preventDefault();
    }
});

window.addEventListener("gamepadconnected", (event) => {
    const gamepadIndex = event.gamepad.index;
    let state = {
        xAxis: 0,
        select: false,
        start: false,
        a: false,
        b: false
    };
    setInterval(() => {
        const gamepad = navigator.getGamepads()[gamepadIndex];
        const xAsis = gamepad.axes[0];
        const start = gamepad.buttons[9].pressed;
        const a = gamepad.buttons[0].pressed;
        const b = gamepad.buttons[1].pressed;
        const select = gamepad.buttons[8].pressed;

        if (xAsis != state.xAxis) {
            state.xAxis = xAsis;
            if (xAsis == -1) {
                previousSlide();
            } else if (xAsis == 1) {
                nextSlide();
            }
        }
        if (start != state.start) {
            state.start = start;
            if (start) {
                startDemo();
            }
        }
        if (a != state.a) {
            state.a = a;
            if (a) {
                if (isShowingVideo()) {
                    toggleVideoPlayState();
                } else {
                    nextSlide();
                }
            }
        }
        if (b != state.b) {
            state.b = b;
            if (b) {
                closeVideo();
            }
        }
        if (select != state.select) {
            state.select = select;
            if (select) {
                toggleMute();
            }
        }
    }, 30);
});

function startTimer() {
    const totalMinutes = 180;
    const remainingTimeElement = document.querySelector("#remaining-time");
    const now = new Date().getTime();
    const endTime = now + totalMinutes * 60 * 1000;
    let hasPlayedWarning = false;
    setInterval(() => {
        // const now = new Date().getTime();
        // const remainingMinutes = Math.floor((endTime - now) / 60000) + 1;
        //
        // if (remainingMinutes == 5 && !hasPlayedWarning) {
        //     soundWarningElement.play();
        //     hasPlayedWarning = true;
        // }
        //
        // remainingTimeElement.innerHTML = remainingMinutes;
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, '0');
        const minutes = String(now.getMinutes()).padStart(2, '0');
        const time = hours + ':' + minutes;
        remainingTimeElement.innerHTML = time;
    }, 1000);
}

function massageSlides() {
  slides.forEach(slide => {
    if (slide.content) {
      slide.content = slide.content.replace(/<code>/g, '<pre><code>');
      slide.content = slide.content.replace(/<\/code>/g, '</code></pre>');
    }
  })
}

massageSlides();
updateCurrentSlide();
startTimer();
