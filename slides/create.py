#!/usr/bin/env python

import json
import markdown

presentation_lines = [line.rstrip() for line in open("presentation.md", "r").readlines()]

slides = []
content = []
slide = {}

skip_line = False
for line in presentation_lines:
    if line.startswith("/*"):
        skip_line = True
        continue
    elif line.startswith("*/"):
        skip_line = False
        continue
    elif skip_line:
        continue

    if line.startswith("# "):
        if slide:
            slides.append(slide)
        slide = {'type': 'section', 'title': line[2:]}
    elif line.startswith("## "):
        if slide:
            slides.append(slide)
        slide = {'type': 'content', 'title': line[3:], 'content': []}
    else:
        if slide:
            if line.startswith('Demo: '):
                parts = line.split(' ')
                slide['demo'] = { 'url': parts[1], 'text': " ".join(parts[2:])}
            else:
                if slide['type'] == 'content':
                    if line.startswith('```'):
                        line = '```'
                    slide['content'].append(line)
                elif slide['type'] == 'section' and len(line) > 0:
                    slide['type'] = 'cover'
                    slide['author'] = line

if slide:
    slides.append(slide)

for slide in slides:
    if 'content' in slide:
        slide['content'] = markdown.markdown('\n'.join(slide['content']))

slides_json = open("slides.js", "w")
slides_json.write("let slides = ")
slides_json.write(json.dumps(slides))
slides_json.write(";")
slides_json.close()
